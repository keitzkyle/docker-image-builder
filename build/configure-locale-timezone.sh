set -eux

cp /tmp/assets/etc/default/keyboard /etc/default/keyboard
# set noninteractive installation
export DEBIAN_FRONTEND=noninteractive
echo "1"
# set your timezone
ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
cp /tmp/assets/etc/default/locale /etc/default/locale
echo "2"
# disable interactive dpkg
echo "debconf debconf/frontend select noninteractive" | debconf-set-selections
# timezone
echo "3"
dpkg-reconfigure --frontend noninteractive -p critical tzdata
echo "4"
# locale
locale-gen en_US.UTF-8
echo "5"
dpkg-reconfigure --frontend noninteractive -p critical libc6
echo "6"
dpkg-reconfigure --frontend noninteractive -p critical locales
echo "7"